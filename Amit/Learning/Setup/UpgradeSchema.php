<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Amit\Learning\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;


/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        if(version_compare($context->getVersion(), '1.0.1','<'))
        {
                 $setup->getConnection()->addColumn(
                    $setup->getTable('learning_sample_item'),
                    'description',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Item description'
                    ]
                );
           
        }
        if(version_compare($context->getVersion(), '1.0.2','<'))
        {
                 $setup->getConnection()->addColumn(
                    $setup->getTable('learning_sample_item'),
                    'image',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'image path'
                    ]
                );
           
        }
        if(version_compare($context->getVersion(), '1.0.3','<'))
        {
                 $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order_grid'),
                    'base_tax_amount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'comment' => 'Base Tax Amount'
                    ]
                );
           
        }

         if(version_compare($context->getVersion(), '1.0.4','<'))
        {
                $setup->getConnection()->addColumn(
                    $setup->getTable('quote'),
                    'cash',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'comment' => 'cash amount'
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order'),
                    'cash',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'comment' => 'cash amount'
                    ]
                );
                 
                $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order_grid'),
                    'cash',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'comment' => 'cash amount'
                    ]
                );
           
        }

        $setup->endSetup();
    }

  
}
