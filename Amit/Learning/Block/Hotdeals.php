<?php
namespace Amit\Learning\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;



class Hotdeals extends \Magento\Catalog\Block\Product\ListProduct
{
	private $productCollectionFactory;
 

  public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,       
      \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
      array $data = []
  )
  {    
      $this->_productCollectionFactory = $productCollectionFactory;    
      
      $this->scopeConfig=$context->getScopeConfig();
      parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
  }


 public function getProductCollection()
{
    $collection = $this->_productCollectionFactory->create();
    $collection->addAttributeToFilter('hotdeals',1);
    //$collection->addAttributeToFilter('hotdeals', array('eq' => 'yes'));
    //$collection->addFieldToFilter(array('attribute'=>'hotdeals','eq'=>'yes'));
    $collection->addAttributeToSelect('*');
    
    return $collection;
}

  
}