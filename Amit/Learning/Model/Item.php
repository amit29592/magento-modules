<?php

namespace Amit\Learning\Model;

use Magento\Framework\Model\AbstractModel;

class Item extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Amit\Learning\Model\ResourceModel\Item::class);
    }
}