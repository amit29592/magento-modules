<?php

namespace Amit\Learning\Controller\Adminhtml;

use Amit\Learning\Model\ItemFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;


class Save extends Action
{
    private $itemFactory;
 
    protected $dataPersistor;
    protected $imageUploader;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ItemFactory $itemFactory,
        DataPersistorInterface $dataPersistor
    ) {
        $this->itemFactory = $itemFactory;
       // $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    public function execute()
    {
        $model = $this->itemFactory->create();
       
        $data = $this->_filterFoodData($data);
       
        $model->setData($data);
        $model->save();
        return $this->resultRedirectFactory->create()->setPath('learning/index/index');

    }
       
    

    public function _filterFoodData(array $rawData)
    {
        
        $data = $rawData;
        if (isset($data['image'][0]['name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = null;
        }
        return $data;
    }
}
