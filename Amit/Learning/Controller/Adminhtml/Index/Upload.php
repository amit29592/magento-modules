<?php
/**
* Simple Hello World Module
*
* @category QaisarSatti
* @package QaisarSatti_HelloWorld
* @author Muhammad Qaisar Satti
* @Email qaisarssatti@gmail.com
*
*/
namespace Amit\Learning\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

use Magento\Backend\App\Action;

/**
 * Class Upload
 */
class Upload extends \Magento\Backend\App\Action
{
    protected $adapterFactory;
/**
* @var \Magento\MediaStorage\Model\File\UploaderFactory
*/
protected $uploader;
/**
* @var \Magento\Framework\Filesystem
*/
protected $filesystem;


    public function __construct(
        Action\Context $context,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Amit\Learning::Image');
    }

    public function execute()
    {
        //start block upload image
        if (isset($_FILES['image']) && isset($_FILES['image']['name']) && strlen($_FILES['image']['name'])) {
        /*
        * Save image upload
        */
        try {
        $base_media_path = 'learning/temp/images';
        $uploader = $this->uploader->create(
        ['fileId' => 'image']
        );
        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        $imageAdapter = $this->adapterFactory->create();
        $uploader->addValidateCallback('image', $imageAdapter, 'validateUploadFile');
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $mediaDirectory = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $result = $uploader->save(
        $mediaDirectory->getAbsolutePath(base_media_path)
        );
        $data['image'] = base_media_path.$result['file'];
        } catch (\Exception $e) {
        if ($e->getCode() == 0) {
        $this->messageManager->addError($e->getMessage());
        }
        }
        } else {
        if (isset($data['image']) && isset($data['image']['value'])) {
        if (isset($data['image']['delete'])) {
        $data['image'] = null;
        $data['delete_image'] = true;
        } elseif (isset($data['image']['value'])) {
        $data['image'] = $data['image']['value'];
        } else {
        $data['image'] = null;
        }
        }
        }
    }
}