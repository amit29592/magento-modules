<?php

namespace Amit\Learning\Controller\Adminhtml\Item;

use Amit\Learning\Model\ItemFactory;
use Magento\Backend\App\Action;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Filesystem\DirectoryList;


class Save extends Action
{
    private $itemFactory;

    /**
    * @var \Magento\MediaStorage\Model\File\UploaderFactory
    */
    protected $_fileUploaderFactory;
    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $filesystem;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ItemFactory $itemFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $_fileUploaderFactory

    ) {
        $this->itemFactory = $itemFactory;
       
        $this->_fileUploaderFactory = $_fileUploaderFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();


        $resultRedirect = $this->resultRedirectFactory->create();


       $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);
         
        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
         
        $uploader->setAllowRenameFiles(false);
         
        $uploader->setFilesDispersion(false);

        $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
         
        ->getAbsolutePath('images/');
         
        $uploader->save($path);
           $this->itemFactory->create()
            ->setData($this->getRequest()->getPostValue()['general'])
            ->save();
        return $this->resultRedirectFactory->create()->setPath('learning/index/index');


    }
       
    

    
}

