<?php

namespace Amit\Learning\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SalesOrderPlaceAfter implements ObserverInterface
{
    private $logger;
    protected $quoteFactory;

    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,   
        LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
    }

    public function execute(Observer $observer)
    {
        $this->logger->debug("Called Observer");
        try {
                $order = $observer->getEvent()->getOrder();
                $order->setCash(70);
              //  $order->save();
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
    }
}
