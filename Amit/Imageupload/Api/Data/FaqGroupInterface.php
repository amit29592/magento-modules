<?php

/**
 * MagePrince
 * Copyright (C) 2018 Mageprince
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html
 *
 * @category MagePrince
 * @package Prince_Faq
 * @copyright Copyright (c) 2018 MagePrince
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author MagePrince
 */

namespace Amit\Imageupload\Api\Data;

interface FaqGroupInterface
{

    const NAME = 'name';
    const ID = 'id';
    const IMAGE = 'image';
   
    /**
     * Get faqgroup_id
     * @return string|null
     */
    
    public function getId();

    /**
     * Set faqgroup_id
     * @param string $faqgroup_id
     * @return \Prince\Faq\Api\Data\FaqGroupInterface
     */
    
    public function setId($faqgroupId);

    /**
     * Get groupname
     * @return string|null
     */
    
    public function getName();

    /**
     * Set groupname
     * @param string $groupname
     * @return \Prince\Faq\Api\Data\FaqGroupInterface
     */
    
    public function setName($groupname);

    public function getImage();

    /**
     * Set icon
     * @param string $icon
     * @return \Prince\Faq\Api\Data\FaqGroupInterface
     */
    
    public function setImage($image);

    }
