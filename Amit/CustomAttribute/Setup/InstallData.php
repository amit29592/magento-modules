<?php

namespace Amit\CustomAttribute\Setup;

use Magento\Eav\Setup\EavSetup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Model\Config;

class InstallData implements InstallDataInterface {

    private $_eavSetupFactory;
    private $_attributeRepository;
    private $eavConfig;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        Config $eavConfig
    )
    {
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_attributeRepository = $attributeRepository;
        $this->eavConfig = $eavConfig;
    }

    public function install( ModuleDataSetupInterface $setup, ModuleContextInterface $context )
    {

        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

        
        if(version_compare($context->getVersion(), '1.0.0','<'))
        {

             $eavSetup->addAttribute(
               \Magento\Customer\Model\Customer::ENTITY, 'customer_attribute4', [

                    'user_defined' => true,
                    'position' =>999,
                    'type' => 'int',
                    'label' => 'Custom Status New',
                    'input' => 'select',
                    'sort_order' => 9999,
                    'source' => 'Amit\CustomAttribute\Model\Config\Source\Options',
                    'global' => 1,
                    'default' => 0,
                    'visible' => true,
                    'required' => false,
                    'system' => false,
                    'group' => 'Account Information',
                    'option' => [ 
                        'values' => [],
                    ]
                ]    
            ); 

            // allow customer_attribute attribute to be saved in the specific areas
            $attribute = $this->_attributeRepository->get('customer', 'customer_attribute4');
            $setup->getConnection()
            ->insertOnDuplicate(
                $setup->getTable('customer_form_attribute'),
                [
                    ['form_code' => 'adminhtml_customer', 'attribute_id' => $attribute->getId()],
                    ['form_code' => 'customer_account_create', 'attribute_id' => $attribute->getId()],
                    ['form_code' => 'customer_account_edit', 'attribute_id' => $attribute->getId()],
                ]
            );

        }
    }
}